#include <stdio.h>
#include "data_sharing_lib.h"

int main()
{
	int shmid;
	service_request_t req;
	msgbuf* req_buf;
	shmid = shmget(900,sizeof(service_request_t),0666|IPC_CREAT);
	req_buf = (msgbuf*)shmat(shmid,NULL,0);
//	req_buf.client_pid = getpid();
	
	req.data = "temp";
	call_service(req,1,900);
	
	return 0;
}

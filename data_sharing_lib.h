#include <math.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
//request types
#define BLOCKING 0
#define NON_BLOCKING 1
//keys
#define SEMAPHORE_KEY 900
#define MSG_QUEUE_KEY 100

#if 1
typedef struct
{
	char* data;	// void* for generic data
	int length_of_data;
	int response;
}service_request_t;
#endif

typedef struct
{
	long msgtype;
	pid_t client_pid;
	int shared_data_key;
	int length_of_data;
	int req_type;
}msgbuf;

static key_t get_mq_key();

void call_service(service_request_t req, int reqType, int shared_key)
{
	printf("\nlength of data : %d, data is: %s\n",(int)req.length_of_data,req.data);	
	msgbuf msg;
	key_t msgq_key = get_mq_key();
	msg.client_pid = getpid();
	msg.msgtype = 0;
	msg.req_type = reqType;
	msg.length_of_data = req.length_of_data;
	msg.shared_data_key = shared_key;
	int msg_qid = msgget(100, 0666);
	int msgsz = sizeof(msg) - sizeof(long);
	printf("header file qid: %d\n",msg_qid);
	if(msg_qid != -1)
	{
		printf("***in if\n");
	  	int send = msgsnd(msg_qid,&msg,msgsz,0);	
		printf("send = %d\n",send);
	}
	return;
} 

key_t get_mq_key()
{
	key_t k = ftok("/tmp",'o');
	if(k == -1)
		return MSG_QUEUE_KEY;
	return k;
} 

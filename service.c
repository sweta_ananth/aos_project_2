#include <stdio.h>
#include "data_sharing_lib.h"
#include "sha256.h"
#define num_worker_threads 2

msgbuf* get_request()//QOS
{
	//get a msg to work on based on QoS
	//create a Q per client and pull request from the right Q (QoS)
	msgbuf* req;	
	int msgsz = sizeof(req1) - sizeof(long); 
	int msg_qid;
       	key_t key = 100;//get_mq_key();
	if((msg_qid = msgget(key, 0666 | IPC_CREAT)) == -1)//gets msgq_id associated with key
	{
		perror("msg get error\n");
	}
	msgrcv(msg_qid,&req,msgsz,0,0);
	return(req);
}

int main()
{
	printf("service qid: %d\n",msg_qid);
	while(1)
	{
		msgbuf* req;	
		req = get_request();//send some bookkeeping stuff to determine QoS
		//check if req is blocking or non blocking
		//blocking => compute here, non blocking => return an indication, spawn a thread
		if(req->req_type == BLOCKING)
		{
			//spawn a thread later if required
			//get the string to work on in str
			char* hash = sha256_wf(str);
			//how is response sent
			//Q?? shared memory??
		}
		else//NON_BLOCKING case
		{
		}
	}
	return 0;
}
